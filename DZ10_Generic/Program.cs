﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork10_Generics
{
    class Team<T>
    {
        public string TeamName { get; set; }
        public T League { get; set; }
    }
    class Player<T> : Team<T>
    {
        public string PlayerName { get; set; }
        public string Position { get; set; }
        public int Salary { get; set; }

        public void Info()
        {
            Console.WriteLine($"Команда: {TeamName} \tЛига: {League}");
            Console.WriteLine($"Футболист: {PlayerName}");
            Console.WriteLine($"Позиция: {Position}");
            Console.WriteLine($"Зарплата: {Salary}");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Team<string>> teamMilan = new List<Team<string>>();
            teamMilan.Add(new Team<string> { TeamName = "Милан", League = "Серия А" });
            teamMilan.Add(new Team<string> { TeamName = "Милан", League = "Лига Чемпионов" });
            List<Player<string>> player = new List<Player<string>>();
            player.Add(new Player<string> { TeamName = teamMilan[0].TeamName, League = teamMilan[0].League , PlayerName = "Златан Ибрагимович", Position = "Нападающий", Salary = 2000000 });
            player.Add(new Player<string> { TeamName = teamMilan[1].TeamName, League = teamMilan[1].League, PlayerName = "Симон Кьер", Position = "Защитник", Salary = 1500000 });

            foreach (Player<string> p in player)
            {
                p.Info();
            }
            Console.WriteLine();
            Console.WriteLine();

            List<Team<string>> teamName = new List<Team<string>>();
            teamName.Add(new Team<string> { TeamName = "Ростов", League = "РФПЛ" });
            teamName.Add(new Team<string> { TeamName = "Ливерпуль", League="АПЛ" });
            teamName.Add(new Team<string> { TeamName = "Реал Мадрид", League = "Примера" });
            List<Player<string>> playerName = new List<Player<string>>();
            playerName.Add(new Player<string> { TeamName = teamName[0].TeamName, League = teamName[0].League, PlayerName = "Сергей Песьяков", Position = "Вратарь", Salary = 500000 });
            playerName.Add(new Player<string> { TeamName = teamName[1].TeamName, League = teamName[1].League, PlayerName = "Мохамед Салллах", Position = "Нападающий", Salary = 4000000 });
            playerName.Add(new Player<string> { TeamName = teamName[2].TeamName, League = teamName[2].League, PlayerName = "Лука Модрич", Position = "Полузащитник", Salary = 2500000 });

            foreach (Player<string> p in playerName)
            {
                p.Info();
            }
        }
    }
}