﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ1_Sortirovka
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int element = 0;
            string str;
            Console.Write("Vvedi kol-vo elementov massiva: ");
            str= Console.ReadLine();
            element = Convert.ToInt32(str);
            Console.Write("Vvedi elementi massiva: ");
            int[] myArray = new int[element];
            for (int i = 0; i < element; i++)
            {
                str = Console.ReadLine();
                myArray[i] = Convert.ToInt32(str);
            }

            Console.Write("Massiv do sortirovki:");
            for (int i=0; i< myArray.Length; i++)
            {
                Console.Write( " " +myArray[i] );
            }
            Console.WriteLine();
            for (int i = 0; i < myArray.Length; i++)
            {
                for (int j = 0; j < myArray.Length - 1; j++)
                {
                    if (myArray[j] > myArray[j + 1])
                    {
                        int k = myArray[j];
                        myArray[j] = myArray[j + 1];
                        myArray[j + 1] = k;
                    }
                }
            }
            Console.Write("Massiv posle sortirovki:");
            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write(" " + myArray[i]);
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
    

