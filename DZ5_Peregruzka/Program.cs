﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ5_Peregruzka
{
    class Complex
    {
        public double Re { get; private set; }
        public double Im { get; private set; }
        public Complex(double r, double i)
        { Re = r; Im = i; }

        public static Complex operator +(Complex x, Complex y)
        { return new Complex(x.Re + y.Re, x.Im + y.Im); }

        public static Complex operator +(double v, Complex x)
        {
            return new Complex(x.Re + v, x.Im);
        }
        public static Complex operator +(Complex x, double v)
        {
            return new Complex(x.Re + v, x.Im);
        }
        public static Complex operator -(Complex x, Complex y)
        { return new Complex(x.Re - y.Re, x.Im - y.Im); }

        public static Complex operator -(double v, Complex x)
        {
            return new Complex(x.Re - v, x.Im);
        }
        public static Complex operator -(Complex x, double v)
        {
            return new Complex(x.Re - v, x.Im);
        }
        public static Complex operator *(Complex x, Complex y)
        { return new Complex(x.Re * y.Re, x.Im * y.Im); }

        public static Complex operator *(double v, Complex x)
        {
            return new Complex(x.Re * v, x.Im);
        }
        public static Complex operator *(Complex x, double v)
        {
            return new Complex(x.Re * v, x.Im);
        }
        public static Complex operator /(Complex x, Complex y)
        { return new Complex(x.Re / y.Re, x.Im / y.Im); }

        public static Complex operator /(double v, Complex x)
        {
            return new Complex(x.Re / v, x.Im);
        }
        public static Complex operator /(Complex x, double v)
        {
            return new Complex(x.Re / v, x.Im);
        }

        public static bool operator ==(Complex x, Complex y)
        {
            return (x.Re == y.Re) && (x.Im == y.Im);
        }
        public static bool operator ==(Complex x, double v)
        {
            return (x.Re == v) && (x.Im == v);
        }
        public static bool operator ==(double v, Complex y)
        {
            return (v == y.Re) && (v == y.Im);
        }
        public static bool operator !=(Complex x, Complex y)
        {
            return (x.Re != y.Re) && (x.Im != y.Im);
        }
        public static bool operator !=(Complex x, double v)
        {
            return (x.Re != v) && (x.Im != v);
        }
        public static bool operator !=(double v, Complex y)
        {
            return (v != y.Re) && (v != y.Im);
        }
        public static bool operator >(Complex x, Complex y)
        {
            return ((x.Re > y.Re) && (x.Im > y.Im));
        }
        public static bool operator >(Complex x, double v)
        {
            return (x.Re > v) && (x.Im > v);
        }
        public static bool operator >(double v, Complex y)
        {
            return (v > y.Re) && (v > y.Im);
        }
        public static bool operator <(Complex x, Complex y)
        {
            return ((x.Re < y.Re) && (x.Im < y.Im));
        }

        public static bool operator <(Complex x, double v)
        {
            return (x.Re < v) && (x.Im < v);
        }
        public static bool operator <(double v, Complex y)
        {
            return (v < y.Re) && (v < y.Im);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Выберите комплексные числа или действительные: \n1 - 2 комплексных числа \n2 - вещественное и комплексное \n3 - комплексное и вещественное");
            var chislo = Convert.ToDouble(Console.ReadLine());
            if (chislo == 1)
            {
                Console.WriteLine("Введите действительную и мнимую часть комплексного числа: ");
                var a = Convert.ToDouble(Console.ReadLine());
                var b = Convert.ToDouble(Console.ReadLine());
                Complex complex1 = new Complex(a, b);
                Console.WriteLine("Введите действительную и мнимую часть комплексного числа ");
                var c = Convert.ToDouble(Console.ReadLine());
                var d = Convert.ToDouble(Console.ReadLine());
                Complex complex2 = new Complex(c, d);
                Console.WriteLine("Выберите и введите операцию сложения (+), вычинатиния (-), умножения (*) или деления (/): ");
                string operation = Console.ReadLine();
                switch (operation)
                {
                    case "+":
                        Complex result1 = complex1 + complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result1.Re, result1.Im);
                        break;
                    case "-":
                        Complex result2 = complex1 - complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result2.Re, result2.Im);
                        break;
                    case "*":
                        Complex result3 = complex1 * complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result3.Re, result3.Im);
                        break;
                    case "/":
                        Complex result4 = complex1 / complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result4.Re, result4.Im);
                        break;
                }
                bool utvergdenie = (complex1 == complex2);
                Console.WriteLine("Первое комплексное число равно второму? - " + utvergdenie);
                bool utvergdenie2 = (complex1 != complex2);
                Console.WriteLine("Первое комплексное число не равно второму? - " + utvergdenie2);
                bool utvergdenie3 = (complex1 < complex2);
                Console.WriteLine("Первое комплексное число меньше второго? - " + utvergdenie3);
                bool utvergdenie4 = (complex1 > complex2);
                Console.WriteLine("Первое комплексное число больше второго? - " + utvergdenie4);
            }
            if (chislo == 2)
            {
                Console.WriteLine("Введите вещественное число: ");
                double a = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите действительную и мнимую часть комплексного числа: ");
                var c = Convert.ToDouble(Console.ReadLine());
                var d = Convert.ToDouble(Console.ReadLine());
                Complex complex2 = new Complex(c, d);
                Console.WriteLine("Выберите и введите операцию сложения (+), вычинатиния (-), умножения (*) или деления (/): ");
                string operation = Console.ReadLine();
                switch (operation)
                {
                    case "+":
                        Complex result1 = a + complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result1.Re, result1.Im);
                        break;
                    case "-":
                        Complex result2 = a - complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result2.Re, result2.Im);
                        break;
                    case "*":
                        Complex result3 = a * complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result3.Re, result3.Im);
                        break;
                    case "/":
                        Complex result4 = a / complex2;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result4.Re, result4.Im);
                        break;
                }
                bool utvergdenie = (a == c);
                Console.WriteLine("Вещественное число равно действительной части комлексного? - " + utvergdenie);
                bool utvergdenie2 = (a != c);
                Console.WriteLine("Вещественное число не равно действительной части комлексного? - " + utvergdenie2);
                bool utvergdenie3 = (a < c);
                Console.WriteLine("Вещественное число меньше действительной части комлексного? - " + utvergdenie3);
                bool utvergdenie4 = (a > c);
                Console.WriteLine("Вещественное число больше действительной части комлексного? - " + utvergdenie4);
            }
            if (chislo == 3)
            {
                Console.WriteLine("Введите действительную и мнимую часть комплексного числа: ");
                var a = Convert.ToDouble(Console.ReadLine());
                var b = Convert.ToDouble(Console.ReadLine());
                Complex complex1 = new Complex(a, b);
                Console.WriteLine("Введите вещественное число: ");
                double c = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Выберите и введите операцию сложения (+), вычинатиния (-), умножения (*) или деления (/): ");
                string operation = Console.ReadLine();
                switch (operation)
                {
                    case "+":
                        Complex result1 = complex1 + c;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result1.Re, result1.Im);
                        break;
                    case "-":
                        Complex result2 = complex1 - c;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result2.Re, result2.Im);
                        break;
                    case "*":
                        Complex result3 = complex1 * c;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result3.Re, result3.Im);
                        break;
                    case "/":
                        Complex result4 = complex1 / c;
                        Console.WriteLine("Результат: действительная часть {0} \tмнимая часть {1} \n ", result4.Re, result4.Im);
                        break;
                }
                bool utvergdenie = (a == c);
                Console.WriteLine("Действительная часть комлексного числа равна вещественному? - " + utvergdenie);
                bool utvergdenie2 = (a != c);
                Console.WriteLine("Действительная часть комлексного числа не равна вещественному? - " + utvergdenie2);
                bool utvergdenie3 = (a < c);
                Console.WriteLine("Действительная часть комлексного числа меньше вещественного? - " + utvergdenie3);
                bool utvergdenie4 = (a > c);
                Console.WriteLine("Действительная часть комлексного числа больше вещественного? - " + utvergdenie4);
            }
            else
                Console.WriteLine("Неправильный выбор");
        }
    }
}
