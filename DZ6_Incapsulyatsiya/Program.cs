﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DZ6_Incapsulyatsiya
{
    class TaxCalculator
    {
        private double getUsTax;
        private double getEUTax;
        private double getChineseTax;

        public void getTaxRate()
        {
            Console.WriteLine("Введите повышающий коэффициент для страны ");
            getUsTax = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите повышающий коэффициент для области: ");
            getEUTax = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите базовую ставку: ");
            getChineseTax = Convert.ToDouble(Console.ReadLine());
            double sum = getUsTax * getEUTax * getChineseTax;
            Console.WriteLine("Стоимость поездки: {0}", sum);
        }
    }


    interface Employee
    {
        void doWork();
    }
    class Company
    {
        List<Employee> employees;

        public virtual List<Employee> getEmployees()
        {
            throw new NotImplementedException();
        }
        public void createSoftware()
        {
            employees = getEmployees();
            foreach (var e in employees)
            {
                e.doWork();
            }
        }
    }
    class GameDevCompany : Company
    {
        public override List<Employee> getEmployees()
        {
            return new List<Employee> { new Designer(), new Artist() };
        }
    }
    class OutSorsingCompany : Company
    {
        public override List<Employee> getEmployees()
        {
            return new List<Employee> { new Tester(), new Programmer() };
        }
    }
    class Designer : Employee
        {
            public void doWork()
            {
                Console.WriteLine("Разрабатывает визуал");
            }
        }
        class Artist : Employee
        {
            public void doWork()
            {
                Console.WriteLine("Рисует оболочку");
            }
        }
        class Programmer : Employee
        {
            public void doWork()
            {
                Console.WriteLine("Разрабатывает код на С#");
            }
        }
        class Tester : Employee
        {
            public void doWork()
            {
                Console.WriteLine("Заводит дефекты");
            }
        }
    

    interface Engine
    {
        void move();
        
    }

    interface Driver
    {
        void navigate();
    }

    public class Transport : Engine, Driver
    {
       
        public void deliver(double destination, string cargo)
        {
            Console.WriteLine("Доставка на {0} км на транспорте: {1}", destination, cargo);
        }
        public void navigate()
        {
            Console.WriteLine("Маршрут");
            Console.WriteLine("1-робот 2-человек");
            int a= Convert.ToInt32(Console.ReadLine());
            if (a == 1) {
                string robot = ("Водитель робот");
                
                    Console.WriteLine( robot);
                
            }
            else {
                string human = ("Водитель человек");
                
                    Console.WriteLine( human);
                
            }
        }

        public void move()
        {
            Console.WriteLine("Вид двигателя");
            Console.WriteLine("1-двигатель внутреннего сгорания 2-электрический двигатель");
            int a = Convert.ToInt32(Console.ReadLine());
            if (a == 1)
            {
                string CombastionEngine = ("Двигатель внутреннего сгорания");
                {
                    Console.WriteLine( CombastionEngine);
                }
            }
            else
            {
                string ElectricEngine = ("Электрический двигатель");
                {
                    Console.WriteLine(ElectricEngine);
                }
            }
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            TaxCalculator taxi = new TaxCalculator();
            taxi.getTaxRate();

            GameDevCompany company = new GameDevCompany();
            company.createSoftware();
            OutSorsingCompany company2 = new OutSorsingCompany();
            company2.createSoftware();

            Transport transport = new Transport();
            transport.deliver(123, "Газель");
            transport.navigate();
            transport.move();
        }
    }
}





