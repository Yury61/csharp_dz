﻿using System;

namespace Strategy
{
    public interface FlyBehavior
    {
        void fly();
    }
    public interface QuackBehavior
    {
        void quack();
    }

    public class FlyWithWings : FlyBehavior
    {
        public void fly()
        {
            Console.WriteLine("Реализация полета");
        }
    }
    public class FlyNoWay : FlyBehavior
    {
        public void fly()
        {
            Console.WriteLine("Не летает");
        }
    }
    public class Quack : QuackBehavior
    {
        public void quack()
        {
            Console.WriteLine("Утка крякает");
        }
    }
    public class Squeak : QuackBehavior
    {
        public void quack()
        {
            Console.WriteLine("Резиновая утка пищит");
        }
    }
    public class MuteQuack : QuackBehavior
    {
        public void quack()
        {
            Console.WriteLine("Не издает звуков");
        }
    }

    public class Duck {
    public FlyBehavior flyBehavior;
    public QuackBehavior quackBehavior;
    public void swim()
        {
            Console.WriteLine("Утка плавает");
        }
        public virtual void display()
        {
            
        }
        public void performQuack ()
        {
            quackBehavior.quack();
        }
        public void performFly()
        {
            flyBehavior.fly();
        }
        public void setFlyBehavior()
        {
            flyBehavior.fly();
            Console.WriteLine("Полет 2");
        }
        public void setQuackBehavior ()
        {
            quackBehavior.quack();
            Console.WriteLine("Квак 2");
        }

    }
    public class MallardDuck: Duck
    {
        public MallardDuck()
        {
            quackBehavior = new Quack();
            flyBehavior = new FlyWithWings();
        }
        public override void display()
        {
            Console.WriteLine(" MallardDuck");
        }
    }
    public class RedheadDuck : Duck
    {
        public RedheadDuck()
        {
            quackBehavior = new Quack();
            flyBehavior = new FlyWithWings();
        }
        public override void display()
        {
            Console.WriteLine("RedheadDuck");
        }
    }
    public class RubberDuck : Duck {
        public RubberDuck()
        {
            quackBehavior = new Squeak();
            flyBehavior = new FlyNoWay();
        }
        public override void display()
        {
            Console.WriteLine("RubberDuck");
        }
    }
    public class DecoyDuck: Duck
    {
        public  DecoyDuck()
        {
            quackBehavior = new MuteQuack();
            flyBehavior = new FlyNoWay();
        }
        public override void display()
        {
            Console.WriteLine("DecoyDuck");
        }
    }

        internal class Program
        {
            static void Main(string[] args)
            {
            Console.WriteLine("Выбери утку 1-Mallard 2-Redhead 3-Rubber 4-Decoy");
            int a = Convert.ToInt32(Console.ReadLine());
            if (a == 1)
            {
             
                Duck mallard = new MallardDuck();
                mallard.performQuack();
                mallard.performFly();
                mallard.display();
            }
            else if (a == 2)
            {
                Duck readhead = new RedheadDuck();
                readhead.performQuack();
                readhead.performFly();
                readhead.display();
            }
            else if (a == 3)
            {
                Duck rubber = new RubberDuck();
                rubber.performQuack();
                rubber.performFly();
                rubber.display();
            }
            else if (a == 4)
            {
                Duck decoy = new DecoyDuck();
                decoy.performQuack();
                decoy.performFly();
                decoy.display();
            }
            else Console.WriteLine("Неверный ввод");
        }
        }
    }
