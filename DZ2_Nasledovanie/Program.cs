﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_Nasledovanie
{
    public class Animals
    {
        public string value = "Dvidautsa ";
        public string value2 = "Givut ";

        public class Mammals : Animals
        {
            public string value3 = "Kormyat molokom ";
            public string value4 = "Givorogdenie ";
            public class Horse : Mammals
            {
                public string value5 = "Imeyut grivu ";
                public string value6 = "Uchastvuyut v skachkah ";
            }
        }
        public class Fish: Animals
            {
            public string value7 = "Imeuyt gabri ";
            public string value8 = "Givut v vode ";
        }
        public class Insect: Animals
        {
            public string value9 = "3 pari nog ";
            public string value10 = "Telo imeet 3 chasti ";
        }
        public class Spiders : Animals
        { 
            public string value11 = "Pletut pautinu ";
            public string value12 = "4 pari nog ";
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            var a = new Animals.Spiders();
            Console.WriteLine(a.value+a.value2+a.value11+a.value12);

            var b = new Animals.Mammals();
            Console.WriteLine(b.value+b.value2+b.value3+b.value4);

            var c = new Animals.Fish();
            Console.WriteLine(c.value+c.value2+c.value7+c.value8);

            var d = new Animals();
            Console.WriteLine(d.value+d.value2);

            var e = new Animals.Insect();
            Console.WriteLine(e.value+e.value2+e.value9+e.value10);

            var f = new Animals.Mammals.Horse();
            Console.WriteLine(f.value+f.value2+f.value3+f.value4+f.value5+f.value6);
        }
    }
}
