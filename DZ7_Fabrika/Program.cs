﻿using System;

namespace DZ7_Fabrika
{
    public interface Button
    {
        void Render();
        void onClick();
    }
    public class WindowsButton : Button
    {
        public void Render()
        {
            Console.WriteLine("Отрисовка кнопки в Windows");
        }
        public void onClick()
        {
            Console.WriteLine("Нажатие кнопки в Windows");
        }
    }
    public class HTMLButton : Button
    {
        public void Render()
        {
            Console.WriteLine("Отрисовка кнопки в HTML");
        }
        public void onClick()
        {
            Console.WriteLine("Нажатие кнопки в HTML");
        }
    }
    public abstract class Dialog
    {
        public abstract Button createButton();

        public void Render()
        {
            Button okButton = createButton();
            okButton.onClick();
            okButton.Render();
        }

    }
    public class WindowsDialog : Dialog
    {
        public override Button createButton() { return new WindowsButton(); }
    }

    public class WebDialog : Dialog
    {
        public override Button createButton() { return new HTMLButton(); }
    }
    class System
    {
       public Dialog dialog;

        public void Choice()
        {
            Console.Write("1-Windows  2-Web: ");
            int a = Convert.ToInt32(Console.ReadLine());
                if (a == 1) 
             dialog = new WindowsDialog(); 
                else if (a == 2) 
             dialog = new WebDialog();
            else { Console.WriteLine("Неправильный выбор"); }
            
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            System system = new System();
            system.Choice();
            system.dialog.Render();
        }
    }
}