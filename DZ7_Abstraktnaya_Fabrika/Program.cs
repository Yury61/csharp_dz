﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ7_Abstraktnaya_Fabrika
{
        public interface Button
        {
             void Rendering();
        }
        public class WinButton : Button
        {
            public void Rendering() { Console.WriteLine("WinButton"); }
        }
        public class MacButton : Button
        {
            public void Rendering() { Console.WriteLine("MacButton"); }
        }
        public interface Checkbox
        {
            void Rendering(); 
        }
        public class WinCheckbox : Checkbox
        {
            public void Rendering() { Console.WriteLine("WinCheckbox"); }
        }
        public class MacCheckbox : Checkbox
        {
            public void Rendering() { Console.WriteLine("MacCheckbox"); }
        }
        public interface GUIFactory
        {
            Button createButton();
            Checkbox createCheckbox();
        }
        public class WinFactory : GUIFactory
        {
            public Button createButton() { return new WinButton(); }
            public Checkbox createCheckbox() { return new WinCheckbox(); }
        }
        public class MacFactory : GUIFactory
        {
            public Button createButton() { return new MacButton(); }
            public Checkbox createCheckbox() { return new MacCheckbox(); }
        }


        public class Application
        {
            private GUIFactory factory;
            private Button button;
            private Checkbox checkbox;


        public Application(GUIFactory factory)
        {
            this.factory = factory;
            button = factory.createButton();
            button.Rendering();
            checkbox = factory.createCheckbox();
            checkbox.Rendering();
        }

           
        class Program
        {
            static void Main(string[] args)
            {
                GUIFactory factory = null;
                Console.WriteLine("Выберите ОС 1-Windows 2-Mac");
                int a = Convert.ToInt32(Console.ReadLine());
                if (a == 1) { factory = new WinFactory(); }
                else if (a == 2) { factory = new MacFactory(); }
                else throw new Exception("Неправильный выбор");
                Application application = new Application(factory);
            }
        }
    }
}