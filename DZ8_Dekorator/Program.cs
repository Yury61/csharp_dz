﻿using System;

namespace HomeWork8_Decorator
{
    interface DataSource
    {
        void writeData(string data );
        void readData();
    }

    class FileDataSource : DataSource
    {
        private string data;
        public FileDataSource(string data)
        {
            this.data = data;
        }
        public void writeData(string data)
        {
            Console.WriteLine($"Filename");
            Console.WriteLine($"Save {data} data");
        }
        public void readData()
        {
            Console.WriteLine($"Read {data} data");
        }
    }
    abstract class DataSourceDecorator : DataSource
    {
        protected DataSource wrappee;
        public DataSourceDecorator(DataSource wrappee)
        {
            this.wrappee = wrappee;
        }
        public virtual void writeData(string data)
        {
            wrappee.writeData(data);
        }
        public virtual void readData()
        {
            return;
        }
    }
    class EncryptionDecorator : DataSourceDecorator
    {
        public EncryptionDecorator(DataSource wrappee) : base(wrappee) { }
        public override void readData()
        {
            Console.WriteLine("Decryption done!");
            wrappee.readData();
        }
        public override void writeData(string data)
        {
            wrappee.writeData(data);
            Console.WriteLine($"Encryption done!");
        }
    }
    class CompressionDecorator : DataSourceDecorator
    {
        public CompressionDecorator(DataSource wrappee) : base(wrappee) { }
        public override void readData()
        {
            Console.WriteLine($"Unpacking done!");
            wrappee.readData();
        }
        public override void writeData(string data)
        {
            wrappee.writeData(data);
            Console.WriteLine($"Convert done!");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            DataSource data1 = new FileDataSource("Before encription");
            DataSourceDecorator encryption1 = new EncryptionDecorator(data1);
            DataSourceDecorator compression1 = new CompressionDecorator(encryption1);
            compression1.writeData("Before encription");
            Console.WriteLine();


            DataSource data2 = new FileDataSource("After encription");
            DataSourceDecorator encryption2 = new EncryptionDecorator(data2);
            DataSourceDecorator compression2 = new CompressionDecorator(encryption2);
            compression2.readData();
        }
    }
}

