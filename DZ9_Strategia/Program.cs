﻿using System;

namespace Strategy
{
        public interface Strategy
        {
            double execute(double a, double b);
        }

        public class ConcreteStrategyAdd : Strategy
        {
            public double execute(double a, double b)
            {
                return a + b;
            }
        }
        public class ConcreteStrategySubtract : Strategy
        {
            public double execute(double a, double b)
            {
                return a - b;
            }
        }
        public class ConcreteStrategyMultiply : Strategy
        {
            public double execute(double a, double b)
            {
                return a * b;
            }
        }

        public class Context
        {
        public Strategy strategy; 

            public void setStrategy(Strategy strategy)
            {
                this.strategy = strategy;
            }
            public double executeStrategy(double a, double b)
            {
                return strategy.execute(a, b);
            }
        internal class Program
        {
            static void Main(string[] args)
            {
                Context context = new Context();
                Console.WriteLine("Введите первое число");
                double a = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите второе число");
                double b = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите операцию: 1: +, 2: -, 3: *");
                int action = Convert.ToInt32(Console.ReadLine());
                if (action == 1) { context.setStrategy(new ConcreteStrategyAdd()); }
                else if (action == 2) { context.setStrategy(new ConcreteStrategySubtract()); }
                else if (action == 3) { context.setStrategy(new ConcreteStrategyMultiply()); }
                else throw new Exception("Неверный ввод.");
                Console.WriteLine("Решение: {0}", context.executeStrategy(a, b));
            }
        }
    }
}