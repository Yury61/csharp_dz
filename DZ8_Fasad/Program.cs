﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork8_Facade
{
    public class VideoFile
    {
        private string Name;
        public VideoFile(string Name)
        { this.Name = Name; }
        public void Filename()
        {
            Console.WriteLine("Download file", this.Name);
        }
    }
    class OggCompressionCodec
    {
       
        public void Compression()
        {
            Console.WriteLine("Archive ogg");
        }
    }
    class MPEG4CompressionCodec
    {
 
        public void Compression()
        {
            Console.WriteLine("Archive mp4");
        }
    }
    class CodecFactory
    {
        public CodecFactory()
        {
      
        }
        public void Codec()
        {
            Console.WriteLine("Select codec");
        }
    }
    public class BitrateReader
    {
        public void read()
        {
            Console.WriteLine("Read bitrate");
        }
        public class BitrateConvert
        {
            private VideoFile file;

            public BitrateConvert(VideoFile file)
            {
                this.file = file;
            }
            public void convert()
            {
                Console.WriteLine("Bitrate change");
            }
        }
        class AudioMixer
        {
            public void mixer()
            {
                Console.WriteLine("Audiomixer change");
            }
        }
        class VideoConverter
        {
            private VideoFile file;
            public VideoConverter(VideoFile file)
            {
                this.file = file;
            }
            public void convert()
            {
                string file = null;
                VideoFile newfile = new VideoFile(file);
                newfile.Filename();
                Console.WriteLine( "What you want? 1-Change format, 2-Change bitrate, 3-Change audio" );
                int select = Convert.ToInt32(Console.ReadLine());
                if (select == 1)
                {
                    Console.WriteLine("Select 1-mp4 or 2-Ogg");
                    int format = Convert.ToInt32(Console.ReadLine());
                    if (format == 1)
                    {
                        MPEG4CompressionCodec distinationCodec = new MPEG4CompressionCodec();
                        distinationCodec.Compression();
                    }
                    else if (format == 2)
                    {
                        OggCompressionCodec distinationCodec = new OggCompressionCodec();
                        distinationCodec.Compression();
                    }
                    else
                    {
                        Console.WriteLine("Incorrect format");
                    }
                    CodecFactory sourceCodec = new CodecFactory();
                    sourceCodec.Codec();
                }
                if (select == 2)
                {
                    BitrateReader bitrateReader = new BitrateReader();
                    bitrateReader.read();
                    BitrateConvert buffer = new BitrateConvert(newfile);
                    buffer.convert();
                }
                if (select == 3)
                {
                    AudioMixer Result = new AudioMixer();
                    Result.mixer();
                }
               
            }
        }
        class Program
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Enter filename (example - funny-cats-video)");
                string namefile;
                namefile=Console.ReadLine();
                VideoFile newfile = new VideoFile(namefile);
                VideoConverter convertor = new VideoConverter(newfile);
                convertor.convert();
            }
        }
    }
}


