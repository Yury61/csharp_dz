﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DZ4_Interface
{
   
        interface IElectricSource : IDisposable
        {
            void Write(string text);
        }
        interface IElectricWire
        {
            void Write(string text, params object[] args);
        }
        interface IElectricAppliance
        {
            void WriteFmt(string fmtText, params object[] args);
        }

        public class ConsoleDebugOutput : IElectricWire, IElectricSource, IElectricAppliance
        {
            void IElectricSource.Write(string text)
            {
                Console.WriteLine(text);

            }
            void IElectricWire.Write(string text, params object[] args)
            {
                Console.WriteLine(text, args);

            }
            void IElectricAppliance.WriteFmt(string fmtText, params object[] args)
            {
                Console.WriteLine(String.Format(fmtText, args));
            }
            public void Dispose() { }
        }

        public abstract class SolarBattery
        {
            public string fuel;
            public SolarBattery(string sun)
            {
                fuel = sun;
            }
        }
        public abstract class DieselGenerator
        {
            public string fuel;
            public DieselGenerator(string diesel)
            {
                fuel = diesel;
            }
        }
        public abstract class NuclearPowerPlant
        {
            public string fuel;
            public NuclearPowerPlant(string uran)
            {
                fuel = uran;
            }
        }

        class Iron : DieselGenerator
        {
            public int Power, Voltage;

            public Iron(int power, int voltage, string diesel) : base(diesel)

            {
                Power = power;
                Voltage = voltage;
            }

            public void Show()
            {
                ConsoleDebugOutput text = new ConsoleDebugOutput();
                IElectricSource source = text;
                IElectricWire wire = text;
                IElectricAppliance appliance = text;

                source.Write("Источник - дизельный генератор");
                wire.Write("Провод - медный с напряжением - {0} В", Voltage);
                appliance.WriteFmt("Прибор - утюг с мощностью - {0} Вт", Power);

            }
        }
    class Refregirator : NuclearPowerPlant
    {
        public int Power, Voltage;

        public Refregirator(int power, int voltage, string uran) : base(uran)

        {
            Power = power;
            Voltage = voltage;
        }

        public void Show2()
        {
            ConsoleDebugOutput text = new ConsoleDebugOutput();
            IElectricSource source = text;
            IElectricWire wire = text;
            IElectricAppliance appliance = text;

            source.Write("\nИсточник - атомная станция");
            wire.Write("Провод - стальной с напряжением - {0} В", Voltage);
            appliance.WriteFmt("Прибор - холодильник с мощностью - {0} Вт", Power);

        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            var iron = new Iron(800, 220, "diesel");
            iron.Show();
            var refregirator = new Refregirator(100, 110, "uran");
            refregirator.Show2();
        }
    }
}