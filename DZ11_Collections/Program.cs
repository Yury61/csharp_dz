﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Observable_collections
{
    class Info
    {
        public string Name { get; set; }
        public string Team { get; set; }
        public string Country { get; set; }

        public void ShowInfo ()
        { Console.WriteLine($"Фамилия имя: {Name}", $"Команда: { Team}", $"Страна: {Country}" ); }
    }    
    internal class Program
    {
        static void Main(string[] args)
        {
            ObservableCollection<Info> footboler = new ObservableCollection<Info>
                {
                    new Info { Name = "Криштиану Роналду", Team = "Манчестер Юнайтед", Country = "Португалия" },
                    new Info { Name = "Лионель Месси", Team="ПСЖ", Country="Аргентина" },
                    new Info { Name = "Златан Ибрагимович", Team = "Милан", Country = "Швеция" }
                };
            foreach (Info info in footboler)
            {
                info.ShowInfo();
            }
            footboler.CollectionChanged += Footboler_CollectionChanged;
            footboler.Add(new Info { Name = "Киллиан Мбапе", Team = "ПСЖ", Country = "Франция" });
            footboler.RemoveAt(1);
            footboler[0] = new Info { Name = "Александр Кержаков", Team = "Зенит", Country = "Россия" };

            foreach (Info info in footboler)
            {
                info.ShowInfo();
            }
        }
        private static void Footboler_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    Info newInfo = e.NewItems[0] as Info;
                    Console.WriteLine($"Added new footbolear: {newInfo.Name} \tTeam: {newInfo.Team} \tCountry {newInfo.Country}");
                    break;
                case NotifyCollectionChangedAction.Remove:
                    Info oldInfo = e.OldItems[0] as Info;
                    Console.WriteLine($"Footbolear has deleted: {oldInfo.Name} \tTeam: {oldInfo.Team} \tCountry {oldInfo.Country}");
                    break;
                case NotifyCollectionChangedAction.Replace: 
                    Info replacedInfo=e.OldItems[0] as Info;
                    Info replacingInfo = e.NewItems[0] as Info;
                    Console.WriteLine($"Footbolear {replacedInfo.Name}\tTeam: {replacedInfo.Team} \tCountry: {replacedInfo.Country} \tвыиграл золотой мяч в прошлом году");
                    Console.WriteLine($"Footbolear {replacingInfo.Name}\tTeam: {replacingInfo.Team} \tCountry: {replacingInfo.Country} \tвыиграл золотой мяч в этом году");
                    break;
            }
        }
    }
}
